#ifndef __DELAY_H__
#define __DELAY_H__

//微秒延时函数
void	delayUs(unsigned int n);

//毫秒延时函数
void	delayMs(unsigned int n);


#endif

