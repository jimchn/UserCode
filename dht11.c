#include "intrins.h"
#include "stc89.h"
#include "dht11.h"
#include "delay.h"
#include "gpio.h"

sbit DHT11_PIN = P2 ^ 4;

#define DHT11_PIN_CLR DHT11_PIN = 0 // DHT11_PIN 引脚拉低
#define DHT11_PIN_SET DHT11_PIN = 1 // DHT11_PIN 引脚拉高

// DHT11 温湿度数据
unsigned char humiHighByte = 0; // 湿度高8位 humiHighByte，湿度的整数
unsigned char humiLowByte = 0;   // 湿度低8位 humiLowByte，湿度的小数
unsigned char tempHighByte = 0; // 温度高8位 tempHighByte，温度的整数
unsigned char tempLowByte = 0;   // 温度低8位 tempLowByte，温度的小数

// 对 DHT11 发送起始信号，并检测 DHT11 的响应
// 主机起始信号：拉低总线 T (18ms < T <30ms)
// DHT11响应信号：83us低 + 87us高
static char dht11Start(void)
{
    unsigned char retry = 0; // 定义变量，重试次数
    DHT11_PIN_SET;
    delayUs(2);
    // 主机拉低(18ms<T<30ms)作为起始信号
    DHT11_PIN_CLR; // 主机拉低总线
    delayMs(20);   // 数据手册典型值为 20ms
    DHT11_PIN_SET; // 主机释放总线，总线由上拉电阻拉高，等待 DHT11 回答
    delayUs(5);    // 主机延时5us，等待响应稳定
    // DHT11_PIN_INPUT; // 主机设为输入，判断从机响应信号

    // DHT11 在收到起始信号后，会回答 83us 低电平 + 87us 高电平

    // 检测 DHT11 的低电平响应
    while (DHT11_PIN && (retry < 120)) // 有低电平会跳出循环 超时会跳出循环
    {
        retry++; // 重试次数 ++
        //   _nop_(); // 一次重试延时 1us，重试次数相当于延时时长
    }
    if (retry >= 120) // 判断是否超时
    {
        return -1; // 超时返回 -1，DHT11 无响应
    }
    else
    {
        retry = 0; // 未超时代表 DHT11 正确拉低电平，继续检测
    }

    // 检测 DHT11 的高电平响应
    while (((!DHT11_PIN) && (retry < 120))) // 有高电平会跳出循环 超时会跳出循环
    {
        retry++; // 重试次数 ++
        // _nop_(); // 一次重试延时 1us，重试次数相当于延时时长
    }
    if (retry >= 120) // 判断是否超时
    {
        return -1; // 超时返回 1，DHT11 无响应
    }
    else
    {
        return 0; // DHT11 正确响应，DHT11 正常应答
    }
}

// 从 DHT11 读取 1Byte 数据（高位先出）
unsigned char read1ByteFromDHT11(void)
{
    unsigned char i;
    unsigned char data time;
    unsigned char dat = 0;

    for (i = 0; i < 8; i++)
    {
        // 数据0:54us低 + 23~27us高
        // 数据1:54us低 + 68~74us高

        while (DHT11_PIN) // 等待 DHT11 低电平，低电平来了说明开始接受数据
            ;
        while (!DHT11_PIN) // 等待 DHT11 高电平，高电平来了说明已经跳过 54us 低电平
            ;

        // 软件延时 40us，跳过数据 0 的高电平时间，40us > 23~27us
        _nop_();
        time = 15;
        while (--time)
            ;

        dat <<= 1;
        if (DHT11_PIN) // 40us 后，如果 DHT11_PIN 还为高说明是数据 1
        {
            dat |= 0x01;
        }
    }

    return dat;
}


// DHT11 结束信号，DHT11 输出 54us 低电平后转为高电平
static char dht11Stop(void)
{
    unsigned char retry = 0; // 定义变量，重试次数
    // 检测 DHT11 的低电平响应
    while (DHT11_PIN && (retry < 120))
    {
        retry++; // 重试次数 ++
    }
    if (retry >= 120) // 判断是否超时
    {
        return -1; // 超时返回 1，DHT11 无响应
    }
    else
    {
        DHT11_PIN_SET; // 主机拉高释放总线
        return 0;
    }
}

// 从 DHT11 读取数据
unsigned char readDataFromDht11(void)
{
    unsigned char humiHighByteCache; // 8bit 湿度整数缓存
    unsigned char humiLowByteCache;  // 8bit  湿度小数缓存
    unsigned char tempHighByteCache; // 8bit  温度整数缓存
    unsigned char tempLowByteCache;  // 8bit  温度小数缓存
    unsigned char checkByteCache;    // 8bit  校验位缓存

    EA = 0;
    dht11Start();                             // DHT11 起始
    humiHighByteCache = read1ByteFromDHT11(); // 获取 8bit 湿度整数
    humiLowByteCache = read1ByteFromDHT11();  // 获取 8bit 湿度小数
    tempHighByteCache = read1ByteFromDHT11(); // 获取 8bit 温度整数
    tempLowByteCache = read1ByteFromDHT11();  // 获取 8bit 温度小数
    checkByteCache = read1ByteFromDHT11();    // 获取 8bit 校验位
    dht11Stop();                              // DHT11 结束
    EA = 1;
    if (checkByteCache == humiHighByteCache + humiLowByteCache + tempHighByteCache + tempLowByteCache)
    {
        humiHighByte = humiHighByteCache; // 写入 8bit 湿度整数
        humiLowByte = humiLowByteCache;   // 写入 8bit 湿度小数
        tempHighByte = tempHighByteCache; // 写入 8bit 温度整数
        tempLowByte = tempLowByteCache;   // 写入 8bit 温度小数
        beepOnce(50);
        return 0;
    }
    else
    {
        return -1;
    }
}

// 获取湿度数据
float getHumiData(void)
{
    return humiHighByte;
}

// 获取温度数据
float getTempData(void)
{
    return tempHighByte + (tempLowByte / 10);
}
