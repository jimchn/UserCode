// #include <stdio.h>
#include "stc89.h"
#include "lcd1602.h"
#include "delay.h"
#include "mode.h"
#include "ds1302.h"
#include "dht11.h"

sbit RS = P2 ^ 7;
sbit RW = P2 ^ 6;
sbit EN = P2 ^ 5;

#define RS_CLR RS = 0 // RS引脚拉低，输入指令
#define RS_SET RS = 1 // RS引脚拉高，输出数据

#define RW_CLR RW = 0 // RW引脚拉低，向LCD写指令或数据
#define RW_SET RW = 1 // RW引脚拉高，从LCD读取信息

#define EN_CLR EN = 0 // EN引脚拉低，EN下降沿(1→0)时执行指令
#define EN_SET EN = 1 // EN引脚拉高，EN=1时读取信息

#define DATAPORT P0 // 8位数据总线引脚

static char displayCache[16];      // 定义显示区域临时存储数组
extern unsigned char time_buf1[8]; // 空年月日时分秒周

// LCD1602判忙
bit LCD1602_Check_Busy(void)
{
    DATAPORT = 0xFF;
    RS_CLR;
    RW_SET;
    EN_CLR;
    delayUs(1);
    EN_SET;
    return (bit)(DATAPORT & 0x80);
}

// 向LCD写指令
void LCD1602_Write_Com(unsigned char com)
{
    // while(LCD_Check_Busy());
    delayMs(5);
    RS_CLR;
    RW_CLR;
    EN_SET;
    DATAPORT = com;
    delayUs(1);
    EN_CLR;
}

// 向LCD写数据
void LCD1602_Write_Data(unsigned char Data)
{
    // while(LCD_Check_Busy());
    delayMs(5);
    RS_SET;
    RW_CLR;
    EN_SET;
    DATAPORT = Data;
    delayUs(1);
    EN_CLR;
}

// LCD1602清屏
void LCD1602_Clear(void)
{
    LCD1602_Write_Com(0x01);
    delayMs(5);
}

// LCD1602指定位置写入字符串
void LCD1602_Write_String(unsigned char x, unsigned char y, unsigned char *s)
{
    if (y == 0)
    {
        LCD1602_Write_Com(0x80 + x);
    }
    else
    {
        LCD1602_Write_Com(0xC0 + x);
    }
    while (*s)
    {
        LCD1602_Write_Data(*s);
        s++;
    }
}

// LCD1602指定位置写数据
void LCD1602_Write_Char(unsigned char x, unsigned char y, unsigned char Data)
{
    if (y == 0)
    {
        LCD1602_Write_Com(0x80 + x);
    }
    else
    {
        LCD1602_Write_Com(0xC0 + x);
    }
    LCD1602_Write_Data(Data);
}

// LCD1602初始化
void lcd1602Init(void)
{
    LCD1602_Write_Com(0x38);
    delayMs(5);
    LCD1602_Write_Com(0x38);
    delayMs(5);
    LCD1602_Write_Com(0x38);
    delayMs(5);
    LCD1602_Write_Com(0x38);
    LCD1602_Write_Com(0x08);
    LCD1602_Write_Com(0x01);
    LCD1602_Write_Com(0x06);
    delayMs(5);
    LCD1602_Write_Com(0x0C);
}

// 清除显示缓存
static void clearDisplayCache()
{
    unsigned char i;
    for (i = 0; i < 16; i++)
    {
        displayCache[i] = ' ';
    }
}

// 更新当前时间到显示缓存
static void updateCurrentTimeToDisplayCache()
{
    clearDisplayCache();
    displayCache[0] = getCurrentTimeData(3) / 10 + 0x30;
    displayCache[1] = getCurrentTimeData(3) % 10 + 0x30;
    displayCache[2] = ':';
    displayCache[3] = getCurrentTimeData(4) / 10 + 0x30;
    displayCache[4] = getCurrentTimeData(4) % 10 + 0x30;
    displayCache[5] = ':';
    displayCache[6] = getCurrentTimeData(5) / 10 + 0x30;
    displayCache[7] = getCurrentTimeData(5) % 10 + 0x30;
}

// 将要设置的时间更新到显示缓存
static void updateNewTimeToDisplayCache()
{
    clearDisplayCache();
    displayCache[0] = getNewTimeData(3) / 10 + 0x30;
    displayCache[1] = getNewTimeData(3) % 10 + 0x30;
    displayCache[2] = ':';
    displayCache[3] = getNewTimeData(4) / 10 + 0x30;
    displayCache[4] = getNewTimeData(4) % 10 + 0x30;
    displayCache[5] = ':';
    displayCache[6] = getNewTimeData(5) / 10 + 0x30;
    displayCache[7] = getNewTimeData(5) % 10 + 0x30;
}

// 更新温湿度到显示缓存
static void updateTHToDisplayCache()
{
    clearDisplayCache();
    displayCache[0] = 'T';
    displayCache[1] = 'E';
    displayCache[2] = ':';
    displayCache[3] = (int)getTempData() / 10 + 0x30; // 十位
    displayCache[4] = (int)getTempData() % 10 + 0x30; // 个位
    displayCache[5] = '.';
    displayCache[6] = (int)(getTempData() * 10) % 10 + 0x30;
    displayCache[7] = ' ';
    displayCache[8] = 'R';
    displayCache[9] = 'H';
    displayCache[10] = ':';
    displayCache[11] = (int)getHumiData() / 10 + 0x30;
    displayCache[12] = (int)getHumiData() % 10 + 0x30;
}

// LCD1602显示内容
void lcd1602Display(void)
{
    switch (getRunningMode()) // 获取当前运行的模式
    {
    // 模式 0：正常显示
    case 0:
        updateCurrentTimeToDisplayCache();        // 更新当前时间到显示缓存
        LCD1602_Write_String(0, 0, displayCache); // 显示第一行
        updateTHToDisplayCache();                 // 更新温湿度到显示缓存
        LCD1602_Write_String(0, 1, displayCache); // 显示第二行
        break;
    // 模式 1：调小时
    case 1:
        LCD1602_Write_String(0, 0, "SET Hour"); // 第一行显示：SET Hour
        updateNewTimeToDisplayCache();
        LCD1602_Write_Char(0, 1, displayCache[0]); // 第二行第一位显示小时的十位
        LCD1602_Write_Char(1, 1, displayCache[1]); // 第二行第二位显示小时的个位
        break;
    // 模式 2：调分钟
    case 2:
        LCD1602_Write_String(0, 0, "SET Minute");  // 第一行显示：SET Minute
        updateNewTimeToDisplayCache();             // 将要设置的时间更新到显示缓存
        LCD1602_Write_Char(0, 1, displayCache[3]); // 第二行第一位显示分钟的十位
        LCD1602_Write_Char(1, 1, displayCache[4]); // 第二行第二位显示分钟的个位
        break;
    // 模式 3：调秒钟（秒调零）
    case 3:
        LCD1602_Write_String(0, 0, "SET Second");  // 第一行显示：SET Second
        updateNewTimeToDisplayCache();             // 将要设置的时间更新到显示缓存
        LCD1602_Write_Char(0, 1, displayCache[6]); // 第二行第一位显示秒钟的十位
        LCD1602_Write_Char(1, 1, displayCache[7]); // 第二行第二位显示秒钟的十位
        break;
    default:
        break;
    }
}
