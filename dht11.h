#ifndef __DHT11_H__
#define __DHT11_H__

unsigned char RH(void);

// 从 DHT11 读取数据
unsigned char readDataFromDht11(void);

// 从DHT11获取湿度数据
float getHumiData(void);

// 从DHT11获取温度数据
float getTempData(void);

#endif
